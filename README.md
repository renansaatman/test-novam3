Teste Nova M3
=============

Este repositório foi criado com o propósito de construir uma aplicação para um banco de dados, a fim de serem executadas as funções essenciais do banco (Create, Read, Update e Delete) com uma aplicação na linguagem PHP.

O que foi utilizado
-------------------

Para que fosse possível o desenvolvimento desse projeto, foi utilizado o ambiente de desenvolvimento XAMPP v5.6.30, fazendo a utilização do servidor *Apache*, através do XAMPP Control Panel v3.2.2. Com o ambiente em pleno funcionamento, foi instalado o administrador de bancos MySQL *phpMyAdmin* para a construção das tabelas utilizadas e suas respectivas ações. Tanto o *phpMyAdmin* quanto o projeto em si foram salvos em pastas dentro do endereço "xampp/htdocs/".

Como executar
-------------

O repositório possui um arquivo "novam3.sql" correspondente ao banco de dados. No momento que o *phpMyAdmin* estiver aberto, deve-se criar um banco com o mesmo nome do arquivo ("novam3"), selecionar esse banco e ir na opção "Importar". Será necessário apenas escolher o arquivo em questão (formato SQL) e Executar.

Banco de dados
--------------

Foi criado um banco de nome "novam3" no servidor local. Para o acesso pelo *phpMyAdmin* foi criado um usuário "root" com a senha aberta. A partir disso, foram criadas duas tabelas iniciais, sendo elas a tabela "produto" (contendo id, nome, código sku, preço, descrição e quantidade) e a tabela "categoria" (contendo id, nome e código). Em seguida foi criada uma tabela "produto_categoria" (contendo id, id_produto e id_categoria) para que fosse feita a associação entre produto e categoria. Seguem os scripts utilizados:

### Criação do Banco de dados:
```sql
create database novam3;
```
### Criação das tabelas:
```sql
CREATE TABLE produto(
	id_produto int not null auto_increment primary key,
	nome_prod varchar(255) not null,
	sku varchar(255) not null,
	preco double not null,
	descricao text not null,
	quantidade int not null
);

CREATE TABLE categoria(
	id_categoria int not null auto_increment primary key,
	codigo int not null,
	nome_categoria varchar(255) not null
);

CREATE TABLE produto_categoria(
	id_prod_cat int not null auto_increment primary key,
	id_produto int not null, 
	id_categoria int not null
);
```
Aplicação em PHP
----------------

Inicialmente foi feita uma divisão do arquivo index.html em 4 arquivos: 

* "header.php" (apenas para a exibição do cabeçalho e menu);
* "home.php" (para a exibição do "miolo" da página);
* "footer.php" (caso fosse criado algum rodapé);
* "scripts.php" (caso fossem usados alguns scripts de *javascript*).

Foram também criadas as *views*: 

* "users.php" (não utilizada);
* "categories.php";
* "products.php";
* "add_category.php";
* "add_product.php".

Além deles, foi criado um arquivo "db.php" para conectar o banco de dados à aplicação e também fazer as consultas necessárias ao banco. Todos esses arquivos são chamados no arquivo "index.php" que se encarrega de fazer uma seleção, de acordo com a página que o usuário queira navegar.

### Exibição das tabelas

Para exibir a tabela de categorias cadastradas foi criada uma *query* no arquivo "db.php" que seleciona todos campos da tabela "categoria" e exibe através de um laço *while()*, utilizando a função *mysqli_fetch_array()*. Isso pode ser visto nesse trecho de código da view "categories.php":

```php
while($linha = mysqli_fetch_array($consulta_categoria)){ ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $linha['nome_categoria'] ?></td>
                            <td><?= $linha['codigo'] ?></td>
                            <td class="box-buttons-table">
                                <a href="?pagina=add_category&editar=<?= $linha['id_categoria']?>" id="edit">
                                    <img src="images/min/icon-category-plus.svg" alt="">
                                </a>
                                <a href="delete_category.php?id_categoria=<?= $linha['id_categoria']?>" class="link-button-table" id="delete">
                                    <img src="images/min/icon-delete.svg" alt="">
                                </a>
                            </td>
                        </tr>
                    <?php $i++;} ?>
```

*Query* utilizada em "db.php":

```php
$query3 = "SELECT * FROM categoria";
$consulta_categoria = mysqli_query($conexao, $query3);
```

O mesmo foi feito com a tabela de produtos, porém foram necessárias duas consultas, sendo uma para exibir os termos da tabela "produto" e outra para exibir as categorias associadas ao produto em questão (através do id do produto), como pode ser visto abaixo:

Arquivo "db.php":

```php
$query1 = "SELECT * FROM produto";
$consulta_produto = mysqli_query($conexao, $query1);

$query2 = "SELECT 
    	categoria.nome_categoria
		FROM produto_categoria
		INNER JOIN categoria ON (
    	categoria.id_categoria = produto_categoria.id_categoria)
    	where produto_categoria.id_produto = ";
```

View "products.php":

```php
while($linha = mysqli_fetch_array($consulta_produto)){?>
	                    <tr>
	                        <td><?= $i ?></td>
	                        <td><?= $linha['nome_prod'] ?></td>
	                        <td><?php 
	                        	$aux = mysqli_query($conexao, $query2.$linha['id_produto']);
	                        	while($cat = mysqli_fetch_array($aux)) {
	                         			 echo $cat['nome_categoria']."<br>"; 
	                         	} ?>		
	                         </td>
	                        <td><?= $linha['sku'] ?></td>
	                        <td><?= $linha['preco'] ?></td>
	                        <td><?= $linha['descricao'] ?></td>
	                        <td><?= $linha['quantidade'] ?></td>
	                        <td class="box-buttons-table">
	                            <a href="?pagina=add_product&editar=<?= $linha['id_produto']?>" id="edit">
                                    <img src="images/min/icon-cart-plus.svg" alt="">
                                </a>
                                <a href="delete_product.php?id_produto=<?= $linha['id_produto']?>" class="link-button-table" id="delete">
	                                <img src="images/min/icon-delete.svg" alt="">
	                            </a>
	                        </td>
	                    </tr>
                    <?php $i++;} ?>
```

### Função Delete

A função *delete* foi feita utilizando o método GET, pegando o id do produto ou categoria selecionado e inserindo o id na *query* utilizada para deletar itens.

View "products.php":

```php
<a href="delete_product.php?id_produto=<?= $linha['id_produto']?>" class="link-button-table" id="delete">
    <img src="images/min/icon-delete.svg" alt="">
</a>
```
"delete_product.php":

```php
$id_produto =  $_GET['id_produto'];

$query = "DELETE FROM produto_categoria WHERE id_produto = $id_produto";

mysqli_query($conexao, $query);

$query = "DELETE FROM produto WHERE id_produto = $id_produto";
mysqli_query($conexao, $query);
```

View "categories.php":

```php
<a href="delete_category.php?id_categoria=<?= $linha['id_categoria']?>" class="link-button-table" id="delete">
    <img src="images/min/icon-delete.svg" alt="">
</a>
```

"delete_category.php":

```php
$id_categoria =  $_GET['id_categoria'];

$query = "DELETE FROM produto_categoria WHERE id_categoria = $id_categoria";

mysqli_query($conexao, $query);

$query = "DELETE FROM categoria WHERE id_categoria = $id_categoria";
mysqli_query($conexao, $query);
```

### Função Insert/Update

As funções de adicionar e editar categoria/produto são bem semelhantes, pois foi criado um formulário que é utilizado pelas duas páginas. A principal diferença entra as duas é que quando a função editar é chamada, ela passa o id do produto/categoria via GET, para que possam aparecer os dados do produto/categoria selecionado e não ter que criar um novo.

#### Função Insert

"insert_category.php": 

```php
$nome_categoria = $_POST['nome_categoria'];
$codigo_categoria = $_POST['codigo_categoria'];

$query = "INSERT INTO categoria(codigo, nome_categoria)
	VALUES ($codigo_categoria, '$nome_categoria')";

mysqli_query($conexao, $query);
```

No caso do *insert* dos produtos, optou-se por fazer checkboxes exibindo todas as categorias já cadastradas. Com isso, foi necessário um foreach para adicionar as relações do "id_produto" com os "id_categoria" selecionados, podendo ser mais de um.

"insert_product.php": 

```php
$id_produto = $_POST['id_produto'];
$nome_produto = $_POST['nome_produto'];
$codigo_sku = $_POST['codigo_sku'];
$preco_produto = $_POST['preco_produto'];
$descricao_produto = $_POST['descricao_produto'];
$quantidade = $_POST['quantidade'];
$id_categoria = $_POST['categoria'];

$query = "INSERT INTO produto(nome_prod, sku, preco, descricao, quantidade)
			VALUES ('$nome_produto', '$codigo_sku', $preco_produto, '$descricao_produto', $quantidade)";

mysqli_query($conexao, $query);

$id_novoproduto = mysqli_insert_id($conexao);

foreach ($id_categoria as $value) {
	$query = "INSERT INTO produto_categoria(id_produto, id_categoria)
			VALUES ($id_novoproduto, $value)";
	mysqli_query($conexao, $query);	
}
```

#### Função Update

Para editar um produto ou categoria, o id do produto/categoria selecionado é passado pelo método GET e assim é dado o *update* no item. 

View "categories.php":

```php
<a href="?pagina=add_category&editar=<?= $linha['id_categoria']?>" id="edit">
    <img src="images/min/icon-category-plus.svg" alt="">
</a>
```

"edit_category.php":

```php
$id_categoria = $_POST['id_categoria'];
$nome_categoria = $_POST['nome_categoria'];
$codigo_categoria = $_POST['codigo_categoria'];

$query = "UPDATE categoria SET nome_categoria = '$nome_categoria', codigo = $codigo_categoria
			WHERE id_categoria = $id_categoria";

mysqli_query($conexao, $query);
```

No caso do produto, por ter associação com a tabela "produto_categoria", optou-se por na hora da edição as relações existentes serem deletadas e novas relações de categorias são criadas na forma de *insert*. Porém a interação com a tabela "produto" segue no mesmo processo que a tabela "categoria" (mostrada anteriormente).

View "products.php":

```php
<a href="?pagina=add_product&editar=<?= $linha['id_produto']?>" id="edit">
    <img src="images/min/icon-cart-plus.svg" alt="">
</a>
```

"edit_product.php":

```php
$id_produto = $_POST['id_produto'];
$nome_prod = $_POST['nome_produto'];
$sku = $_POST['codigo_sku'];
$preco = $_POST['preco_produto'];
$descricao = $_POST['descricao_produto'];
$quantidade = $_POST['quantidade'];
$id_categoria = $_POST['categoria'];

$query = "DELETE FROM produto_categoria WHERE id_produto = $id_produto ";
mysqli_query($conexao, $query);

$query = "UPDATE produto SET nome_prod = '$nome_prod', sku = '$sku', preco = $preco, descricao = '$descricao',
			quantidade = $quantidade
			WHERE id_produto = $id_produto";

mysqli_query($conexao, $query);

foreach ($id_categoria as $value) {
	$query = "INSERT INTO produto_categoria(id_produto, id_categoria)
			VALUES ($id_produto, $value)";
	mysqli_query($conexao, $query);	
}
```