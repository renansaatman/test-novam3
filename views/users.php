<table class="table-system" cellpadding="15px">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Name</td>
                        <td>Function</td>
                        <td>Email</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    <?php for($i=1; $i<20; $i++): ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td>Heberty Carlos</td>
                        <td>Web Standart</td>
                        <td>heberty@m3mediadigital.com.br</td>
                        <td class="box-buttons-table">
                            <a href="javascript;;" class="link-button-table" id="active">
                                <img src="images/min/icon-active.svg" alt="">
                            </a>
                            <a href="javascript;;" class="link-button-table" id="desable">
                                <img src="images/min/icon-desable.svg" alt="">
                            </a>
                            <a href="javascript;;" class="link-button-table" id="delete">
                                <img src="images/min/icon-delete.svg" alt="">
                            </a>
                        </td>
                    </tr>
                    <?php endfor; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td>Id</td>
                        <td>Name</td>
                        <td>Function</td>
                        <td>Email</td>
                        <td>Actions</td>
                    </tr>
                </tfoot>
            </table>
        </div>