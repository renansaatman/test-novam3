<div class="row">
                <div class="col-12">
                    <div class="box-dashboard">
                        <h1 class="title-dashboard">Usuários</h1>
                        <div class="diviser-dashboard"></div>
                        <div class="row">
                            <div class="col-12 col-lg-4">
                                <div class="box-user">
                                    <div class="image-user" style="background-image: url('images/min/user.svg');">
                                        <img src="images/min/user.svg" alt="">
                                    </div>
                                    <strong class="name-user">Heberty Carlos</strong>
                                    <p class="function-user">Web Standard</p>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="box-user">
                                    <div class="image-user" style="background-image: url('images/min/user.svg');">
                                        <img src="images/min/user.svg" alt="">
                                    </div>
                                    <strong class="name-user">Heberty Carlos</strong>
                                    <p class="function-user">Web Standard</p>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="box-user">
                                    <div class="image-user" style="background-image: url('images/min/user.svg');">
                                        <img src="images/min/user.svg" alt="">
                                    </div>
                                    <strong class="name-user">Heberty Carlos</strong>
                                    <p class="function-user">Web Standard</p>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="box-user">
                                    <div class="image-user" style="background-image: url('images/min/user.svg');">
                                        <img src="images/min/user.svg" alt="">
                                    </div>
                                    <strong class="name-user">Heberty Carlos</strong>
                                    <p class="function-user">Web Standard</p>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="box-user">
                                    <div class="image-user" style="background-image: url('images/min/user.svg');">
                                        <img src="images/min/user.svg" alt="">
                                    </div>
                                    <strong class="name-user">Heberty Carlos</strong>
                                    <p class="function-user">Web Standard</p>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="box-user">
                                    <div class="image-user" style="background-image: url('images/min/user.svg');">
                                        <img src="images/min/user.svg" alt="">
                                    </div>
                                    <strong class="name-user">Heberty Carlos</strong>
                                    <p class="function-user">Web Standard</p>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="box-user">
                                    <div class="image-user" style="background-image: url('images/min/user.svg');">
                                        <img src="images/min/user.svg" alt="">
                                    </div>
                                    <strong class="name-user">Heberty Carlos</strong>
                                    <p class="function-user">Web Standard</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="box-dashboard">
                        <h1 class="title-dashboard">Categorias</h1>
                        <div class="diviser-dashboard"></div>
                        <table class="table-system" cellpadding="15px">
                            <thead>
                                <tr>
                                    <td>Id</td>
                                    <td>Categoria</td>
                                    <td>Código</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $i = 1;
                                while($linha = mysqli_fetch_array($consulta_categoria)){ ?>
                                    <tr>
                                        <td><?= $i ?></td>
                                        <td><?= $linha['nome_categoria'] ?></td>
                                        <td><?= $linha['codigo'] ?></td>
                                    </tr>
                                <?php $i++;} ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>Id</td>
                                    <td>Categoria</td>
                                    <td>Código</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="box-dashboard">
                        <h1 class="title-dashboard">Produtos</h1>
                        <div class="diviser-dashboard"></div>
                        <table class="table-system" cellpadding="15px">
                            <thead>
                                <tr>
                                    <td>Id</td>
                                    <td>Produto</td>
                                    <td>Categoria</td>
                                    <td>SKU</td>
                                    <td>Preço</td>
                                    <td>Quantidade</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $i = 1;
                                while($linha = mysqli_fetch_array($consulta_produto)){?>
                                    <tr>
                                        <td><?= $i ?></td>
                                        <td><?= $linha['nome_prod'] ?></td>
                                        <td><?php 
                                            $aux = mysqli_query($conexao, $query2.$linha['id_produto']);
                                            while($cat = mysqli_fetch_array($aux)) {
                                                     echo $cat['nome_categoria']."<br>"; 
                                            } ?>        
                                         </td>
                                        <td><?= $linha['sku'] ?></td>
                                        <td><?= $linha['preco'] ?></td>
                                        <td><?= $linha['quantidade'] ?></td>
                                    </tr>
                                <?php $i++;} ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>Id</td>
                                    <td>Produto</td>
                                    <td>Categoria</td>
                                    <td>SKU</td>
                                    <td>Preço</td>
                                    <td>Quantidade</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>