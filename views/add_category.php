<?php if(!isset($_GET['editar'])) { ?>
	<h1 class="title-dashboard">Adicionar Categoria</h1><br>
	<form method="post" action="insert_category.php">
		<h4><label class="badge badge-secondary">Categoria:</label></h4>
		<input class="form-control" type="text" name="nome_categoria" placeholder="Insira o nome da categoria">
		<br>
		<h4><label class="badge badge-secondary">Código:</label></h4>
		<input class="form-control" type="text" name="codigo_categoria" placeholder="Insira o código da categoria">
		<br>
		<input class="btn btn-secondary btn-lg btn-block" type="submit" value="Adicionar">
	</form>
<?php } else{ ?>
	<?php while($linha = mysqli_fetch_array($consulta_categoria)){ ?>
		<?php if($linha['id_categoria'] == $_GET['editar']){ ?>
			<h1 class="title-dashboard">Editar Categoria</h1><br>
			<form method="post" action="edit_category.php">
				<input type="hidden" name="id_categoria" value="<?= $linha['id_categoria'] ?>">
				<h4><label class="badge badge-secondary">Categoria:</label></h4>
				<input class="form-control" type="text" name="nome_categoria" placeholder="Insira o nome da categoria" value="<?= $linha['nome_categoria'] ?>">
				<br>
				<h4><label class="badge badge-secondary">Código:</label></h4>
				<input class="form-control" type="text" name="codigo_categoria" placeholder="Insira o código da categoria" value="<?= $linha['codigo'] ?>">
				<br>
				<input class="btn btn-secondary btn-lg btn-block" type="submit" value="Editar">
			</form>
		<?php } ?>
	<?php } ?>
<?php } ?>