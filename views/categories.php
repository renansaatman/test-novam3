<table class="table-system" cellpadding="15px">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Categoria</td>
                        <td>Código</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $i = 1;
                    while($linha = mysqli_fetch_array($consulta_categoria)){ ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $linha['nome_categoria'] ?></td>
                            <td><?= $linha['codigo'] ?></td>
                            <td class="box-buttons-table">
                                <a href="?pagina=add_category&editar=<?= $linha['id_categoria']?>" id="edit">
                                    <img src="images/min/icon-category-plus.svg" alt="">
                                </a>
                                <a href="delete_category.php?id_categoria=<?= $linha['id_categoria']?>" class="link-button-table" id="delete">
                                    <img src="images/min/icon-delete.svg" alt="">
                                </a>
                            </td>
                        </tr>
                    <?php $i++;} ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td>Id</td>
                        <td>Categoria</td>
                        <td>Código</td>
                        <td>Actions</td>
                    </tr>
                </tfoot>
            </table>
            <a class="btn btn-secondary btn-lg" href="?pagina=add_category" style="position: fixed;">
                Adicionar Categoria
            </a>
        </div>
