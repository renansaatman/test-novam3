<?php if(!isset($_GET['editar'])) { ?>
		<h1 class="title-dashboard">Adicionar Produto</h1><br>
		<form method="post" action="insert_product.php">
			<h4><label class="badge badge-secondary">Produto:</label></h4>
			<input class="form-control" type="text" name="nome_produto" placeholder="Insira o nome do produto">
			<br><br>
			<h4><label class="badge badge-secondary">SKU:</label></h4>
			<input class="form-control" type="text" name="codigo_sku" placeholder="Insira o código SKU do produto">
			<br><br>
			<h4><label class="badge badge-secondary">Preço:</label></h4>
			<input class="form-control" type="text" name="preco_produto" placeholder="Insira o preço do produto">
			<br><br>
			<h4><label class="badge badge-secondary">Descrição:</label></h4>
			<input class="form-control" type="text" name="descricao_produto" placeholder="Insira uma descrição para o produto">
			<br><br>
			<h4><label class="badge badge-secondary">Quantidade:</label></h4>
			<input class="form-control" type="text" name="quantidade" placeholder="Insira a quantidade disponível do produto">
			<br><br>
			
			<h4><label class="badge badge-secondary">Categoria:</label></h4>
			<?php while($linha = mysqli_fetch_array($consulta_categoria)){ ?>
				<input type="checkbox" name="categoria[]" value="<?=$linha['id_categoria']?>"><?= $linha['nome_categoria'] ?><br>
			<?php } ?>
			<br>
			<input class="btn btn-secondary btn-lg btn-block" type="submit" value="Adicionar">
		</form>
<?php } else{ ?>
	<?php while($linha = mysqli_fetch_array($consulta_produto)){ ?>
			<?php if($linha['id_produto'] == $_GET['editar']){ ?>
				<h1 class="title-dashboard">Editar Produto</h1><br>
				<form method="post" action="edit_product.php">
					<input type="hidden" name="id_produto" value="<?= $linha['id_produto'] ?>">
					<h4><label class="badge badge-secondary">Produto:</label></h4>
					<input class="form-control" type="text" name="nome_produto" placeholder="Insira o nome do produto" value="<?= $linha['nome_prod'] ?>">
					<br><br>
					<h4><label class="badge badge-secondary">SKU:</label></h4>
					<input class="form-control" type="text" name="codigo_sku" placeholder="Insira o código SKU do produto" value="<?= $linha['sku'] ?>">
					<br><br>
					<h4><label class="badge badge-secondary">Preço:</label></h4>
					<input class="form-control" type="text" name="preco_produto" placeholder="Insira o preço do produto" value="<?= $linha['preco'] ?>">
					<br><br>
					<h4><label class="badge badge-secondary">Descrição:</label></h4>
					<input class="form-control" type="text" name="descricao_produto" placeholder="Insira uma descrição para o produto" value="<?= $linha['descricao'] ?>">
					<br><br>
					<h4><label class="badge badge-secondary">Quantidade:</label></h4>
					<input class="form-control" type="text" name="quantidade" placeholder="Insira a quantidade disponível do produto" value="<?= $linha['quantidade'] ?>">
					<br><br>
						
					<h4><label class="badge badge-secondary">Categoria:</label></h4>
					<?php while($linha = mysqli_fetch_array($consulta_categoria)){ ?>
						<input type="checkbox" name="categoria[]" value="<?=$linha['id_categoria']?>"><?= $linha['nome_categoria'] ?><br>
					<?php } ?>
					<br>
					<input class="btn btn-secondary btn-lg btn-block" type="submit" value="Editar">
				</form>
		<?php } ?>
	<?php } ?>
<?php } ?>