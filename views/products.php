<table class="table-system" cellpadding="15px">
                <thead>
                    <tr>
                    	<td>Id</td>
                        <td>Produto</td>
                        <td>Categoria</td>
                        <td>SKU</td>
                        <td>Preço</td>
                        <td>Descrição</td>
                        <td>Quantidade</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $i = 1;
                    while($linha = mysqli_fetch_array($consulta_produto)){?>
	                    <tr>
	                        <td><?= $i ?></td>
	                        <td><?= $linha['nome_prod'] ?></td>
	                        <td><?php 
	                        	$aux = mysqli_query($conexao, $query2.$linha['id_produto']);
	                        	while($cat = mysqli_fetch_array($aux)) {
	                         			 echo $cat['nome_categoria']."<br>"; 
	                         	} ?>		
	                         </td>
	                        <td><?= $linha['sku'] ?></td>
	                        <td><?= $linha['preco'] ?></td>
	                        <td><?= $linha['descricao'] ?></td>
	                        <td><?= $linha['quantidade'] ?></td>
	                        <td class="box-buttons-table">
	                            <a href="?pagina=add_product&editar=<?= $linha['id_produto']?>" id="edit">
                                    <img src="images/min/icon-cart-plus.svg" alt="">
                                </a>
                                <a href="delete_product.php?id_produto=<?= $linha['id_produto']?>" class="link-button-table" id="delete">
	                                <img src="images/min/icon-delete.svg" alt="">
	                            </a>
	                        </td>
	                    </tr>
                    <?php $i++;} ?>
                </tbody>
                <tfoot>
                    <tr>
                    	<td>Id</td>
                        <td>Produto</td>
                        <td>Categoria</td>
                        <td>SKU</td>
                        <td>Preço</td>
                        <td>Descrição</td>
                        <td>Quantidade</td>
                        <td>Actions</td>
                    </tr>
                </tfoot>
            </table>
            <a class="btn btn-secondary btn-lg" href="?pagina=add_product" style="position:fixed;">
                Adicionar Produto
            </a>
        </div>

