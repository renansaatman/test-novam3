<?php

# inclui o banco de dados
include 'db.php';
# inclui o cabeçalho
include 'header.php';
# conteúdo da página
if(isset($_GET['pagina'])){
	$pagina = $_GET['pagina'];
}
else{
	$pagina = 'home';
}
# navegação entre páginas
switch($pagina){
	case 'products': include 'views/products.php'; break;
	case 'add_product': include 'views/add_product.php'; break;
	case 'categories': include 'views/categories.php'; break;
	case 'add_category': include 'views/add_category.php'; break;
	case 'users': include 'views/users.php'; break;
	default: include 'views/home.php';break;
}
# inclui o rodapé
include 'footer.php';
# scripts js
include 'scripts.php';
