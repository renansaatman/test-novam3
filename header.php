<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Test Developer PHP</title>
        <meta charset="utf-8">
        <meta name="author" content="Nova M3">
        <meta name="description" content="Nova M3">
        <meta name="keywords" content="Nova M3">
        <!-- <meta name="theme-color" content="#db5945"> -->

        <!-- Css -->
        <link rel="stylesheet" href="css/app.css">

        <!-- View Port -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        
        <div class="wrap push">
            <header class="header-dashboard">
                <a href="#menu" class="menu-link"><img src="images/min/menu-open.svg" alt=""></a>
                <h2 class="title-dashboard">Test Developer PHP</h2>
            </header>
        </div>
        <nav id="menu" class="panel" role="navigation">
            <a href="?pagina=home" class="logo-dashboard">
                <img src="images/min/icon-logo-novam3.svg" alt=""><h1>Test Developer PHP</h1>
            </a>
            <ul>
                <li class="diviser-list"></li>
                <li>
                    <a href="#">
                        <img src="images/min/icon-user-plus.svg" alt="">
                        Adicionar Usuários
                    </a>
                </li>
                <li class="diviser-list"></li>
                <li>
                    <a href="?pagina=users">
                        <img src="images/min/icon-user.svg" alt=""> 
                        Usuários
                    </a>
                </li>
                <li class="diviser-list"></li>
                <li>
                    <a href="?pagina=add_category">
                        <img src="images/min/icon-category-plus.svg" alt=""> 
                        Adicionar Categoria
                    </a>
                </li>
                <li class="diviser-list"></li>
                <li>
                    <a href="?pagina=categories">
                        <img src="images/min/icon-category.svg" alt=""> 
                        Categorias
                    </a>
                </li>
                <li class="diviser-list"></li>
                <li>
                    <a href="?pagina=add_product">
                        <img src="images/min/icon-cart-plus.svg" alt=""> 
                        Adicionar Produtos
                    </a>
                </li>
                <li class="diviser-list"></li>
                <li>
                    <a href="?pagina=products">
                        <img src="images/min/icon-cart.svg" alt=""> 
                        Produtos
                    </a>
                </li>
                <li class="diviser-list"></li>
            </ul>
        </nav>

        <!-- <a href="javascript:;" class="link-float" data-toggle="modal" data-target="#exampleModal">
            <img src="images/min/icon-add-plus.svg" alt="">
        </a> -->
        <div class="dashboard wrap push">
        	<div class="box-units">
                <ul>
                    <li>
                        <a href="?pagina=home">
                            <div class="d-flex align-items-center mb10">
                                <img src="images/min/icon-user.svg" alt="">
                                <span class="name-units">Início</span>
                            </div>
                            <!-- <span class="number-units">100</span> -->
                        </a>
                    </li>
                    <li class="diviser-list"></li>
                    <li>
                        <a href="?pagina=users">
                            <div class="d-flex align-items-center mb10">
                                <img src="images/min/icon-user.svg" alt="">
                                <span class="name-units">Usuários</span>
                            </div>
                            <!--<span class="number-units">100</span> -->
                        </a>
                    </li>
                    <li class="diviser-list"></li>
                    <li>
                        <a href="?pagina=categories">
                            <div class="d-flex align-items-center mb10">
                                <img src="images/min/icon-category.svg" alt="">
                                <span class="name-units">Categorias</span>
                            </div>
                            <!-- <span class="number-units">10</span> -->
                        </a>
                    </li>
                    <li class="diviser-list"></li>
                    <li>
                        <a href="?pagina=products">
                            <div class="d-flex align-items-center mb10">
                                <img src="images/min/icon-cart.svg" alt="">
                                <span class="name-units">Produtos</span>
                            </div>
                            <!-- <span class="number-units">9.999</span> -->
                        </a>
                    </li>
                    <li class="diviser-list"></li>
                    <!-- 
                    <li>
                        <a href="javascript:;" class="add-button">
                            <div class="d-flex align-items-center mb10">
                                <img src="images/min/icon-add-plus.svg" alt="">
                                <span class="name-units">Add button</span>
                            </div> -->
                            <!-- <span class="number-units">9.999</span> -->
                    <!--    </a>
                    </li>
                    <li class="diviser-list"></li>
                    <li>
                        <a href="javascript:;" class="add-button">
                            <div class="d-flex align-items-center mb10">
                                <img src="images/min/icon-add-plus.svg" alt="">
                                <span class="name-units">Add button</span>
                            </div> -->
                            <!-- <span class="number-units">9.999</span> -->
                       <!-- </a>
                    </li> -->
                </ul>
            </div>